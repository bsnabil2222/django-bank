from django.contrib import admin

from .models import Withdrawal
 
class WithdrawalAdmin(admin.ModelAdmin):
    list_display = ('code_cpt','amount','date_of_creation',)
    list_filter = ('date_of_creation',)



#affichage tout les chapms dans admin panel
admin.site.register(Withdrawal, WithdrawalAdmin)
