from django.db import models
from accounts.models import Account
from django.core.validators import MaxValueValidator
from django.utils import timezone

class Withdrawal(models.Model):
    code_cpt = models.ForeignKey(Account, on_delete=models.CASCADE)
    amount = models.FloatField(validators=[MaxValueValidator(500)])  # <--max de retrait 5000

    date_of_creation = models.DateField(default=timezone.now)

    #ef __str__(self):
     #   return f"Withdrawal of {self.amount} from Account {self.Account.code_cpt}"
    
    
    #def save(self):
     #   account = Account.objects.first()  
      #  account.amount = account.amount - self.amount
       # account.save()
        #super().save()