from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import Transaction

class TransactionAdmin(admin.ModelAdmin):
    list_display = ('sender', 'receiver', 'amount')  
    list_filter = ('date_of_creation',)



admin.site.register(Transaction, TransactionAdmin)