from django.db import models
from accounts.models import Account
from django.utils import timezone

class Transaction(models.Model):
    sender = models.ForeignKey(Account, related_name='sender_transactions', on_delete=models.CASCADE)
    receiver = models.ForeignKey(Account, related_name='receiver_transactions', on_delete=models.CASCADE)
    amount = models.FloatField()
    date_of_creation = models.DateField(default=timezone.now)


    def __str__(self):
        return f"Transaction from {self.sender.code_cpt} to {self.receiver.code_cpt}, Amount: {self.amount}"
# foction pour calcule le process de transaction
    def process_transaction(self):
        self.sender.balance -= self.amount
        self.sender.save()
        self.receiver.balance += self.amount
        self.receiver.save()
        self.save()
 