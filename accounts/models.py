from django.db import models
from client.models import Client
import random

from django.core.validators import MinValueValidator
from django.utils import timezone
class Account(models.Model):
    code_cpt = models.IntegerField(primary_key=True,unique=True, default=random.randint(10000, 99999))
    code_cli = models.ForeignKey(Client ,on_delete=models.CASCADE)
   
   
    amount = models.FloatField(validators=[MinValueValidator(250)])
    date_of_creation = models.DateField(default=timezone.now)

  
