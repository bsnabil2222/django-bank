from django.contrib import admin


from .models import Account
class AcountAdmin(admin.ModelAdmin):
    list_display = ('code_cli', 'code_cpt', 'amount','date_of_creation')
    list_filter = ('code_cpt',)



#affichage tout les chapms dans admin panel
admin.site.register(Account, AcountAdmin)