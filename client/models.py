from django.db import models
import random

class Client(models.Model):
    code_cli = models.IntegerField(primary_key=True, unique=True, default=random.randint(1000, 9999))
    fname = models.CharField(max_length=100)
    lname = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.fname} {self.lname}"