from django.contrib import admin
from .models import Client

class ClientAdmin(admin.ModelAdmin):
    list_display = ('code_cli', 'fname', 'lname')
    list_filter = ('code_cli',)
admin.site.register(Client, ClientAdmin)
