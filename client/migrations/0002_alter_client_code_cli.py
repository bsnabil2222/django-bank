# Generated by Django 3.2.25 on 2024-04-13 16:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='code_cli',
            field=models.IntegerField(default=5220, primary_key=True, serialize=False, unique=True),
        ),
    ]
